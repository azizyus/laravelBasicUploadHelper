<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 21.06.2018
 * Time: 04:00
 */

class CheckImageSavedAndDeletedTest  extends \Tests\TestCase
{


    /** @test  */
    public function test()
    {

        $request = new \Illuminate\Http\Request();

        $file = \Illuminate\Http\UploadedFile::fake()->image("test.jpg",250,250);
        $fileName = \Azizyus\LaravelBasicUploadHelper\UploadHelper::uploadFile($file);
        $filePath = public_path(\Azizyus\LaravelBasicUploadHelper\UploadHelper::returnRawUploadDir($fileName));

       ## SAVE FILE AND CHECK IS EXIST ##
        if(file_exists($filePath))
        {
            //file is successfully saved
            $this->assertTrue(true);
        }
        else $this->assertTrue(false);
       ## SAVE FILE AND CHECK IS EXIST ##


       ## DELETE SAVED FILE AND CHECK IS DELETED ##
        \Illuminate\Support\Facades\File::delete($filePath);
        if(file_exists($filePath))
        {
            //file is successfully deleted
            $this->assertTrue(false);
        }
        else $this->assertTrue(true);
       ## DELETE SAVED FILE AND CHECK IS DELETED ##



    }

}