<?php
/**
 * Created by PhpStorm.
 * User: can_r
 * Date: 1/17/2018
 * Time: 3:06 PM
 */

namespace Azizyus\LaravelBasicUploadHelper;


use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Intervention\Image\Image as ImageInstance;
class UploadHelper
{


    /**
     * @param $model
     * @param $imageColumn
     * @param Request $request
     * @param $fileInputName
     * @return mixed
     */

    public static $uploadDir;

    /**
     * @param $file
     */

    //$file == $request->file("fileInputName");
    public static function uploadFile($file)
    {


        $uploadDir = static::$uploadDir;
        $newFileName = md5($file.time()).".{$file->getClientOriginalExtension()}";


        $file->move(public_path($uploadDir),$newFileName);



        return $newFileName;


    }


    public static function returnRawUploadDir($file)
    {

        return "/".static::$uploadDir."/$file";

    }

    public static function uploadFileAndDeleteOldFile($file,$oldImagePath)
    {

        $newFileName = static::uploadFile($file);


        File::delete(public_path(static::$uploadDir."/$oldImagePath"));

        return $newFileName;



    }

    public static function removeAllImageVersions($model,$imageColumn)
    {
        File::delete(static::$uploadDir."/".$model->$imageColumn);
        File::delete(static::$uploadDir."/".static::originalImagePrefixer($model->$imageColumn));
        File::delete(static::$uploadDir."/".static::mediumImagePrefixer($model->$imageColumn));
        File::delete(static::$uploadDir."/".static::smallImagePrefixer($model->$imageColumn));
        File::delete(static::$uploadDir."/".static::dataTableImagePrefixer($model->$imageColumn));
    }

    public static function mediumImageProcess(ImageInstance $image,$savePath)
    {
        $image->fit(520,390)->save($savePath);
    }

    public static function smallImageProcess(ImageInstance $image,$savePath)
    {
        $image->fit(144,118)->save($savePath);
    }
    public static function dataTableImageProcess(ImageInstance $image,$savePath)
    {
        $image->fit(72,59)->save($savePath);
    }

    public static function catchSingleImageAndPutModelPropertyThenDeleteOldImage($model, $imageColumn, Request $request, $fileInputName)
    {



        if($request->hasFile($fileInputName))
        {

            $imageFile = $request->file($fileInputName);

            $image = Image::make($imageFile);




            $imageExtension  = $imageFile->extension();
            $randomFileName = Str::random(20);
            $imagePath = "$randomFileName.$imageExtension";


            static::removeAllImageVersions($model,$imageColumn);

            $originalImagePath = static::$uploadDir."/".static::originalImagePrefixer($imagePath);
            $image->save($originalImagePath);
            static::mediumImageProcess(Image::make($originalImagePath),static::$uploadDir."/".static::mediumImagePrefixer($imagePath));
            static::smallImageProcess(Image::make($originalImagePath),static::$uploadDir."/".static::smallImagePrefixer($imagePath));
            static::dataTableImageProcess(Image::make($originalImagePath),static::$uploadDir."/".static::dataTableImagePrefixer($imagePath));



            return $imagePath;


        }
        else return $model->$imageColumn;




    }

    public static function smallImagePrefixer($string,$withUploadDir=false)
    {

        if($withUploadDir) return "/".static::$uploadDir."/small-$string";
        return "small-$string";

    }

    public static function mediumImagePrefixer($string,$withUploadDir=false)
    {
        if($withUploadDir) return "/".static::$uploadDir."/medium-$string";
        return "medium-$string";

    }

    public static function originalImagePrefixer($string,$withUploadDir=false)
    {
        if($withUploadDir) return "/".static::$uploadDir."/original-$string";
        return "original-$string";

    }

    public static function dataTableImagePrefixer($string,$withUploadDir=false)
    {
        if($withUploadDir) return "/".static::$uploadDir."/datatable-$string";
        return "datatable-$string";

    }

}