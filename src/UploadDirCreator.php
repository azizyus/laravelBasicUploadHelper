<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 06.06.2018
 * Time: 05:19
 */


namespace Azizyus\LaravelBasicUploadHelper;


class UploadDirCreator
{


    public function createDirIfDoesntExist($uploadDir=null) : Bool
    {

        if($uploadDir==null)
            $uploadDirToCheck=UploadHelper::$uploadDir;
        else
            $uploadDirToCheck=$uploadDir;


        if(!file_exists(public_path($uploadDirToCheck)))
        {

            mkdir(public_path($uploadDirToCheck));
            return true;
        }

        return false;
    }
}