<?php

namespace Azizyus\LaravelBasicUploadHelper;


use Illuminate\Support\ServiceProvider;

class UploadHelperProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {



        $this->publishes([


            __DIR__."/config/config.php" => config_path("upload_helper.php")

        ],"upload_helper");






    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        UploadHelper::$uploadDir = config("upload_helper.upload_dir");


        $uploaDirCreator = new UploadDirCreator();
        $uploaDirCreator->createDirIfDoesntExist();


    }
}
